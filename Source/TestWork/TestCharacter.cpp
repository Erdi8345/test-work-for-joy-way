// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCharacter.h"

#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Interfaces/InventoryInterface.h"
#include "Interfaces/InteractInterface.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ATestCharacter::ATestCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Create camera component and settings...
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	Camera->SetupAttachment(GetRootComponent());
	Camera->bUsePawnControlRotation = true;

	//Create hand component...
	HandComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HandComponent"));
	HandComponent->SetupAttachment(GetCameraComponent());
	
	//Create inventory component...
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));

	//Create health component...
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

// Called to bind functionality to input
void ATestCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	
	// Bind UseItem events
    PlayerInputComponent->BindAction("UseItem", IE_Pressed, this, &ATestCharacter::OnUseItem);
    PlayerInputComponent->BindAction("UseItem", IE_Released, this, &ATestCharacter::OnStopUseItem);

	// Bind fire event
	//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ATestinputProjectCharacter::OnFire);

	//Bind interact event
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ATestCharacter::Interact);
	
	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ATestCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATestCharacter::MoveRight);

	// "turn" handles devices that provide an absolute delta, such as a mouse.
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Bind toggle inventory event
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, this, &ATestCharacter::ToggleInventory);
}

void ATestCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ATestCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ATestCharacter::Interact()
{
	FVector StartLocation = GetCameraComponent()->GetComponentLocation();
	const FVector EndLocation = StartLocation + (GetControlRotation().Vector() * InteractDistance);
	FHitResult ResultTrace;
	bool HitSuccess = UKismetSystemLibrary::LineTraceSingleForObjects(this, StartLocation, EndLocation, ObjectTypesInteract, true, TArray<AActor*> {this}, bDebugTrace ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None, ResultTrace, true);
	if(HitSuccess && ResultTrace.GetActor())
	{
		if (ResultTrace.GetActor()->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
		{
			IInteractInterface::Execute_Interact(ResultTrace.GetActor(), this);
		}
	}
}

void ATestCharacter::ToggleInventory()
{
	if(auto PC = GetController<APlayerController>())
	{
		if(!InventoryWidget)
		{
			InventoryWidget = CreateWidget(PC, InventoryWidgetClass);
			if(InventoryWidget->GetClass()->ImplementsInterface(UInventoryInterface::StaticClass()))
			{
				IInventoryInterface::Execute_InitializationWidget(InventoryWidget);
			}
		}
		
		if(InventoryWidget->IsInViewport())
		{
			InventoryWidget->RemoveFromViewport();
			PC->SetInputMode(FInputModeGameOnly());
			PC->bShowMouseCursor = false;
		}
		else
		{
			PC->bShowMouseCursor = true;
			FInputModeGameAndUI InputModeGameAndUI;
			InputModeGameAndUI.SetWidgetToFocus(InventoryWidget->TakeWidget());
			PC->SetInputMode(InputModeGameAndUI);
			InventoryWidget->AddToViewport();
		}
	}
}

void ATestCharacter::OnUseItem()
{
	if(auto Item = InventoryComponent->GetSelectedItem())
	{
		Item->UseItem();
	}
}

void ATestCharacter::OnStopUseItem() 
{
	if(auto Item = InventoryComponent->GetSelectedItem())
	{
		Item->StopUseItem();
	}
}

void ATestCharacter::OnReload()
{
	if(auto Item = Cast<ABaseWeaponItem>(InventoryComponent->GetSelectedItem()))
	{
		Item->ReloadWeapon();
	}
}