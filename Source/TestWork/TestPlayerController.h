// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRespawnCharacter, APawn*, NewCharacter);

/**
 * 
 */
UCLASS()
class TESTWORK_API ATestPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable)
	FOnRespawnCharacter OnRespawnCharacter;
	
	/** Widget class for spawn inventory */
	UPROPERTY(EditAnywhere, category = "UI")
	TSubclassOf<UUserWidget> DeadScreenWidgetClass;
	
	UPROPERTY(EditAnywhere, category = "UI")
	TSubclassOf<UUserWidget> MainScreenWidgetClass;
	
	/** Widget inventory */
	UPROPERTY()
	UUserWidget* DeadScreenWidget;
	
	/** MainScreen inventory */
	UPROPERTY()
	UUserWidget* MainScreenWidget;
	
	virtual void OnPossess(APawn* InPawn) override;
	
	void ToggleMenu();

	UFUNCTION()
	void CharacterDead();
	
	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	UFUNCTION(BlueprintCallable)
	void RespawnCharacter();

	virtual void BeginPlay() override;
};
