// Fill out your copyright notice in the Description page of Project Settings.

#include "TestWork/Objects/BasePickItem.h"
#include "Components/InventoryComponent.h"
#include "GameFramework/Character.h"

// Sets default values
ABasePickItem::ABasePickItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Create root component...
	MyRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("MyRootComponent"));
	SetRootComponent(MyRootComponent);
	
	//Create mesh component...
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("mesh"));
	Mesh->SetupAttachment(GetRootComponent());
}

bool ABasePickItem::Interact_Implementation(ACharacter* Player)
{
	if(Player)
	{
		//Get inventory from character
		if(auto Inventory = Cast<UInventoryComponent>(Player->GetComponentByClass(UInventoryComponent::StaticClass())))
		{
			Inventory->AddItem(this);
		}
	}
	return true;
}