// Fill out your copyright notice in the Description page of Project Settings.

#include "Objects/Weapons/BaseWeaponItem.h"

#include "BulletPoolingManager.h"
#include "TestCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/Controller.h"

ABaseWeaponItem::ABaseWeaponItem()
{
	//Create shoot socket component...
	ShootSocket = CreateDefaultSubobject<USceneComponent>(TEXT("ShootSocket"));
	ShootSocket->SetupAttachment(Mesh);
}

void ABaseWeaponItem::UseItem()
{
	Super::UseItem();

	if(AbleToUseItem())
	{
		CurrentAmmo--;
		GetWorldTimerManager().SetTimer(CooldownTimer, this, &ABaseWeaponItem::StopRateDelay, UseRate);
		if(bAutoFire)
		{
			bFireActive = true;
		}
		UKismetSystemLibrary::PrintString(this, "Shoot!");

		//Shoot Projectile
		{
			FHitResult HitResult;
			bool bHit = CalculateShootTrace(HitResult);

			FVector FireLocation(ShootSocket->GetComponentLocation());
			FRotator DirectRotation = UKismetMathLibrary::FindLookAtRotation(FireLocation, HitResult.GetActor() ? HitResult.ImpactPoint : HitResult.TraceEnd);
			
			UBulletPoolingManager* PoolingManager = GetWorld()->GetSubsystem<UBulletPoolingManager>();

			if (!PoolingManager)
			{
				return;
			}

			FTransform LTransform(DirectRotation, FireLocation, FVector(1.f));
			PoolingManager->SafeSpawnBulletActor(Bullet, LTransform);

			if (ABaseBulletProjectile* BulletActor = PoolingManager->GetBulletActorByClass(Bullet))
			{
				BulletActor->SetOwner(GetOwner());
				BulletActor->SetWeaponOwner(this);
				BulletActor->SetPoolingActive(false);
				BulletActor->SetActorLocation(FireLocation);
				BulletActor->SetActorRotation(DirectRotation);
				BulletActor->InitLocation(FireLocation);
				BulletActor->SetIgnoreActors(TArray<AActor*> { this, GetOwner() } );
				BulletActor->SetLifeSpan(10.f);
				BulletActor->Init(FBulletData(Damage));
				BulletActor->SetPoolingActive(true);
			}
		}
		
		if(CurrentAmmo <= 0 && TotalAmmo > 0)
		{
			ReloadWeapon();
		}
		
		OnShoot.Broadcast();
	}
}

void ABaseWeaponItem::StopUseItem()
{
	Super::StopUseItem();

	bFireActive = false;
}

void ABaseWeaponItem::ReloadWeapon()
{
	if(!bReloading)
	{
		bReloading = true;
		GetWorldTimerManager().SetTimer(ReloadTimer, this, &ABaseWeaponItem::FinishReload, ReloadTime);
		AmmoToReload = FMath::Min(GetClass()->GetDefaultObject<ABaseWeaponItem>()->GetCurrentAmmo() - CurrentAmmo, TotalAmmo);
	}
}

ATestCharacter* ABaseWeaponItem::GetCharacterOwner()
{
	if(!OwnerCharacter)
	{
		OwnerCharacter = Cast<ATestCharacter>(GetOwner());
	}

	return OwnerCharacter;
}

void ABaseWeaponItem::StopRateDelay()
{
	GetWorldTimerManager().ClearTimer(CooldownTimer);
	if(bAutoFire && bFireActive)
	{
		UseItem();
	}
}

void ABaseWeaponItem::FinishReload()
{
	bReloading = false;
	GetWorldTimerManager().ClearTimer(ReloadTimer);
	CurrentAmmo = CurrentAmmo + AmmoToReload;
	TotalAmmo = TotalAmmo - AmmoToReload;
}

bool ABaseWeaponItem::CalculateShootTrace(FHitResult& HitResult)
{
	FVector StartTrace = GetCharacterOwner()->GetCameraComponent()->GetComponentLocation();
	FVector EndTrace = StartTrace + (GetCharacterOwner()->GetControlRotation().Vector() * 500.f);
	
	return UKismetSystemLibrary::LineTraceSingleForObjects(this, StartTrace, EndTrace,
		ObjectTypes, true, TArray<AActor*> { this, GetOwner() }, EDrawDebugTrace::None,
		HitResult, true);
}