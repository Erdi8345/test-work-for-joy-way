// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data/DataAssets/ImpactDataAsset.h"
#include "GameFramework/Actor.h"
#include "BaseBulletProjectile.generated.h"

USTRUCT(BlueprintType)
struct FBulletData
{
	GENERATED_BODY()

public:
	FBulletData()
	{
		BaseDamage = 0;
	}

	FBulletData(float InDamage)
	{
		BaseDamage = InDamage;
	}

	UPROPERTY()
	float BaseDamage;

	
};

class ABaseWeaponItem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBulletDamageActor, float, Damage, AActor*, DamageActor);

UCLASS()
class TESTWORK_API ABaseBulletProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseBulletProjectile();

protected:
	
	UPROPERTY(VisibleAnywhere)
	class UBulletProjectileMoveComponent* ProjectileMovement;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent* Collider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* BulletMesh;
	
	UPROPERTY(VisibleAnywhere)
	UAudioComponent* AudioComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bHideOnHit;
	
	FBulletData BulletData;
	
	UImpactDataAsset* GetImpactDataAsset(const FHitResult& IHit);
	
	void DeactivatePooling();
	
public:
	FBulletDamageActor OnDamageActor;

	/** Initializes the initial data */
	void Init(FBulletData IBulletData) { BulletData = IBulletData; }
	
	//~ Begin AActor interface
	virtual void SetLifeSpan(float InLifespan) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	//~ End AActor interface

	virtual void SetPoolingActive(bool bInActive);
	bool IsPoolingActive() const;

	void InitSound(UObject* Sound, float Volume, float Pitch, bool bIs2D, class USoundAttenuation* AttenuationOverride = nullptr, class USoundConcurrency* ConcurrencyOverride = nullptr);
	void InitLocation(FVector Location) { StartLocation = Location; }

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void SetIgnoreActors(TArray<AActor*> Actors);

	virtual void SetWeaponOwner(ABaseWeaponItem* InWeaponOwner) { WeaponOwner = InWeaponOwner; };
	
protected:
	
	float PoolingLifeSpan;
	FTimerHandle PoolingTimer;
	bool bPoolingActive;
	bool bUseNiagara;
	FVector StartLocation;
	
	UPROPERTY()
	ABaseWeaponItem* WeaponOwner = nullptr;
};
