// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/BasePickItem.h"
#include "BaseWeaponItem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShoot);

/**
 * 
 */
UCLASS()
class TESTWORK_API ABaseWeaponItem : public ABasePickItem
{
	GENERATED_BODY()

public:
	ABaseWeaponItem();
	
	UPROPERTY(BlueprintAssignable)
	FShoot OnShoot;
	
protected:

	/** Socket to understand where the bullet should come from */
	UPROPERTY(VisibleAnywhere)
	USceneComponent* ShootSocket;
	
	/** The entire ammunition of the weapon */
	UPROPERTY(EditAnywhere, Category = "Weapon")
	int32 TotalAmmo = 100;

	/** Can the weapon fire automatically? */
	UPROPERTY(EditAnywhere, Category = "Weapon")
	bool bAutoFire;

	/** The interval between shots when firing automatically */
	UPROPERTY(EditAnywhere, meta = (EditCondition = bAutoFire), Category = "Weapon")
	float UseRate;

	/** Weapon reloading time */
	UPROPERTY(EditAnywhere, Category = "Weapon")
	float ReloadTime;

	/** Data impact information */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon|ImpactSettings")
	TMap<TEnumAsByte<EPhysicalSurface>, class UImpactDataAsset*> DataAsset;

	/** What objects can be hit by a shot trace */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;

	/** Bullet class */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<class ABaseBulletProjectile> Bullet;
	
	/** Damage */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
    float Damage;
	
	UPROPERTY(BlueprintReadOnly)
	int32 CurrentAmmo = 30;
	
	FTimerHandle CooldownTimer;
	FTimerHandle ReloadTimer;
	bool bFireActive;
	bool bReloading;
	int AmmoToReload;

	UPROPERTY()
	class ATestCharacter* OwnerCharacter;
	
	virtual void UseItem() override;

	virtual void StopUseItem() override;
	
	virtual void StopRateDelay();

	virtual void FinishReload();

	bool CalculateShootTrace(FHitResult& HitResult);

public:
	virtual void ReloadWeapon();
	
	int32 GetCurrentAmmo() const { return CurrentAmmo; }

	UFUNCTION(BlueprintCallable)
	TMap<TEnumAsByte<EPhysicalSurface>, class UImpactDataAsset*>& GetDataAsset() { return DataAsset; }

protected:
	ATestCharacter* GetCharacterOwner();
};
