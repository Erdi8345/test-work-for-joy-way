// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/InteractInterface.h"
#include "BasePickItem.generated.h"

UCLASS()
class TESTWORK_API ABasePickItem : public AActor, public IInteractInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickItem();

	UPROPERTY(VisibleAnywhere)
	USceneComponent* MyRootComponent;
	
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	/** Item icon */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	UTexture2D* Icon;
	
	/** Item name */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
	FText Name;

	/** Can the item be used? */
	virtual bool AbleToUseItem() { return true; }
	
	/** Use this item */
	UFUNCTION(BlueprintCallable)
	virtual void UseItem() { /* Use item */ }

	/** Stop use this item if possible */
	virtual void StopUseItem() { /* Stop use item */ };
	
	//BEGIN Interact interface
	virtual bool Interact_Implementation(ACharacter* Player) override;
	//END Interact interface
};
