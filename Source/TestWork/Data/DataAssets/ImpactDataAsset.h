// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ImpactDataAsset.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TESTWORK_API UImpactDataAsset : public UDataAsset
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(EditAnywhere, Category = "ImpactSound")
	TArray<class USoundBase*> ImpactSounds;

	/** Sound value multiplayer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ImpactSound", AdvancedDisplay)
	float VolumeMultiplayer = 1.f;

	/** Sound speed multiplayer */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ImpactSound", AdvancedDisplay)
	float PitchMultiplayer = 1.f;
	
	UFUNCTION()
	USoundBase* GetRandomSound();
	
};
