// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/InventoryComponent.h"
#include "TestCharacter.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	if(auto HealthComponent = Cast<UHealthComponent>(GetOwnerCharacter()->GetComponentByClass(UHealthComponent::StaticClass())))
	{
		HealthComponent->CharDead.AddDynamic(this, &UInventoryComponent::OwnerDead);
	}
}

void UInventoryComponent::OwnerDead()
{
	for(auto Item : Items)
	{
	    Item->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Item->SetActorLocation(GetOwnerCharacter()->GetActorLocation());
	    Item->SetActorEnableCollision(true);
	    Item->SetActorHiddenInGame(false);
	    Item->SetOwner(nullptr);
	    
	    OnInventoryUpdate.Broadcast(Item, Items.Find(Item), Remove);
	}

	Items.Empty();
}

void UInventoryComponent::AddItem(ABasePickItem* Item)
{
	if(Item)
	{
		Item->SetActorHiddenInGame(true);
		Item->SetActorEnableCollision(false);
		Item->SetActorLocation(FVector(0.f));
		Item->SetOwner(GetOwner());
		OnInventoryUpdate.Broadcast(Item, Items.AddUnique(Item), Add);
	}
}

void UInventoryComponent::RemoveItem(int32 Id)
{
	if(Items.IsValidIndex(Id))
	{
		const auto DroppedItem = Items[Id];
		DroppedItem->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		const FVector DropLocation = GetOwnerCharacter()->GetActorLocation() + (GetOwnerCharacter()->GetActorForwardVector() * 40.f);
		DroppedItem->SetActorLocation(DropLocation);
		DroppedItem->SetActorEnableCollision(true);
		DroppedItem->SetActorHiddenInGame(false);
		DroppedItem->SetOwner(nullptr);
		Items.Remove(DroppedItem);
		OnInventoryUpdate.Broadcast(DroppedItem, Id, Remove);
	}
}

void UInventoryComponent::SelectItem(int32 ItemIndex)
{
	if(Items.IsValidIndex(ItemIndex))
	{
		if(SelectedItem)
		{
			SelectedItem->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
			SelectedItem->SetActorHiddenInGame(true);
			SelectedItem->SetActorEnableCollision(false);
			SelectedItem->SetActorLocation(FVector(0.f));
		}
		
		SelectedItem = Items[ItemIndex];
		SelectedItem->SetActorEnableCollision(false);
		SelectedItem->SetActorHiddenInGame(false);
		
		SelectedItem->AttachToComponent(GetOwnerCharacter()->GetHandComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale);
		OnSelectedItem.Broadcast(SelectedItem);
	}
}

ATestCharacter* UInventoryComponent::GetOwnerCharacter()
{
	if(!OwnerCharacter)
	{
		OwnerCharacter = Cast<ATestCharacter>(GetOwner());
	}

	return OwnerCharacter;
}