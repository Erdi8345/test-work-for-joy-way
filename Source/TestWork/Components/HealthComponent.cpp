// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/HealthComponent.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UHealthComponent::GetHealth(float& Max, float& Current) const
{
	Max = MaxHealth;
	Current = Health;
}

void UHealthComponent::ResetHealth()
{
	// ...
	Health = MaxHealth;
	
	OnChangeHealth.Broadcast(Health, GetName());
}

void UHealthComponent::OnPlayerAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	Health = FMath::Max(Health - Damage, 0.f);
	OnChangeHealth.Broadcast(Health, GetName());
	if (Health <= 0.f && !bIsDead)
	{
		bIsDead = true;
		UKismetSystemLibrary::PrintString(GetOwner(), "Dead");
		CharDead.Broadcast();

		//Clear all delegates
		OnChangeHealth.Clear();
		CharDead.Clear();
	}
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	ResetHealth();

	if(GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnPlayerAnyDamage);
	}
}