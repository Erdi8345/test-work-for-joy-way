// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Objects/Weapons/BaseWeaponItem.h"
#include "InventoryComponent.generated.h"

UENUM(BlueprintType)
enum EUpdateRequest
{
	Add,
	Remove
};

class ATestCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSelectedItemUpdate, ABasePickItem*, NewItem);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FInventoryUpdate, ABasePickItem*, Item, int32, Id, EUpdateRequest, UpdateRequest);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTWORK_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();

	/** Called when selecting a new item */
	UPROPERTY(BlueprintAssignable)
	FSelectedItemUpdate OnSelectedItem;

	/** Called at any inventory change */
	UPROPERTY(BlueprintAssignable)
	FInventoryUpdate OnInventoryUpdate;
	
protected:

	/** All items in inventory */
	UPROPERTY(BlueprintReadOnly)
	TArray<ABasePickItem*> Items;

	/** Current selected item */
	UPROPERTY(BlueprintReadOnly)
	ABasePickItem* SelectedItem;

	UPROPERTY()
	ATestCharacter* OwnerCharacter;
	
public:	

	virtual void BeginPlay() override;
	
	UFUNCTION()
	void OwnerDead();

	/** Add item in inventory */
	UFUNCTION(BlueprintCallable)
	void AddItem(ABasePickItem* Item);
	
	/** Remove item from inventory */
	UFUNCTION(BlueprintCallable)
	void RemoveItem(int32 Id);

	/** Select new item by item index */
	UFUNCTION(BlueprintCallable)
	void SelectItem(int32 ItemIndex);

	/** Return all items */
	UFUNCTION(BlueprintCallable)
	TArray<ABasePickItem*> GetItems() { return Items; }
	
	/** Return current selected item */
	ABasePickItem* GetSelectedItem() const { return SelectedItem; }

protected:
	/** Return cast owner character */
	UFUNCTION(BlueprintCallable)
	ATestCharacter* GetOwnerCharacter();
};
