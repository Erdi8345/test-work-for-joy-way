// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "BulletProjectileMoveComponent.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API UBulletProjectileMoveComponent : public UProjectileMovementComponent
{
	GENERATED_BODY()

public:
	/** Restarts the bullet simulation  */
	void RestartSimulation();

	/** Stops bullet simulation */
	void StopSimulating(const FHitResult& HitResult) override;

	/** Setup a new component of the collision  */
	void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;

	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool IsStopped() const { return bStoppedSimulation; }
	
private:
	bool bStoppedSimulation;
};
