// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletPoolingManager.h"

void UBulletPoolingManager::DestroyBulletPool(const UObject* WorldContextObject)
{
	if (!GEngine) { return; }

	if (const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		if (UBulletPoolingManager* PoolingManager = World->GetSubsystem<UBulletPoolingManager>())
		{
			PoolingManager->DestroyBulletActors();
		}
	}
}

void UBulletPoolingManager::SafeSpawnBulletActor(TSubclassOf<ABaseBulletProjectile> Class, FTransform Trans)
{
	if (!GetWorld()) { return; }
		
	if (BulletActors.Num() < 2000)
	{
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		ABaseBulletProjectile* BulletActor = GetWorld()->SpawnActor<ABaseBulletProjectile>(Class, FTransform(FRotator(0.f), 
			FVector(0.f), FVector(1.f)), SpawnParams);
		
		BulletActors.Add(BulletActor);
	}
}

void UBulletPoolingManager::DestroyBulletActors()
{
	for (ABaseBulletProjectile* PooledActor : BulletActors)
	{
		if (PooledActor)
		{
			PooledActor->Destroy();
		}
	}

	BulletActors.Empty();
}

ABaseBulletProjectile* UBulletPoolingManager::GetBulletActorByClass(TSubclassOf<ABaseBulletProjectile> BulletClass)
{
	for (ABaseBulletProjectile* PooledActor : BulletActors)
	{
		if (PooledActor && !PooledActor->IsPoolingActive() && PooledActor->IsA(BulletClass))
		{
			return PooledActor;
		}
	}

	if (BulletActors.Num() > 0)
	{
		ABaseBulletProjectile* PooledActor = BulletActors[0];
		BulletActors.Add(PooledActor);
		BulletActors.RemoveAt(0);
		return PooledActor;
	}

	return nullptr;
}